# README #

### Information Design 2016S Semester Project ###

Visualizing a random forest, a machine learning algorithm for the classification problem

Group: H

* Alexander Hauer (0825190)
* Christoph Kindl (0828633)
* Christoph Presch (0602554)

We want to visualize a Machine Learning classification algorithm called "Random Forest".
A random forest consists of a bunch of decision trees which are created with different
portions of the training set using different (randomized) parameters. The final decision
is based on the majority vote of all trees.

### Setup ###

In the project's root directory, run `npm install` to get all required dependencies.