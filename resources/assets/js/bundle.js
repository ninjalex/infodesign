/*
 |--------------------------------------------------------------------------
 | Frameworks, Common Helper Libraries & Plugins
 |--------------------------------------------------------------------------
 */
window.Vue                   = require('vue');
window.$ = window.jQuery     = require('jquery');
window._ = window.underscore = require('underscore');
window.seedrandom            = require('seedrandom');
window.vis                   = require('vis');
require('bootstrap-sass');

var WebFont = require('webfontloader');
WebFont.load({
    google: {
        families: ['Open Sans', 'Montserrat']
    }
});

require('./extend.js');
window.ml = require('./ml.js');
window.util = require('./util.js');

/*
 |--------------------------------------------------------------------------
 | Vue.js, Plugins & Components
 |--------------------------------------------------------------------------
 */
Vue.config.debug = true;

new Vue({
    el: '#infodesign-app',
    components: {
        'main': require('./components/Main.vue')
    }
});
