"use strict";

/**
 * This file extends standard JS objects with custom convenient functions.
 */

/**
 * Returns the last element of an array.
 * @returns {*}
 */
Array.prototype.last = function() {
    return this[this.length - 1];
};

/**
 * Returns the sum of all elements within the array.
 */
Array.prototype.sum = function() {
    if (this.some(isNaN)) {
        throw new Error("Array to sum up must contain numeric elements only!");
    }
    return this.reduce((pv, cv) => pv + cv, 0);
};

/**
 * Returns a copy of the array.
 * @returns {Array}
 */
Array.prototype.clone = function() {
    return this.slice(0);
};

/**
 * Returns an array containing the difference between the current array and 'that'.
 * @param that the other array
 * @returns {Array}
 */
Array.prototype.diff = function(that) {
    return this.filter((e) => that.indexOf(e) < 0);
};

/**
 * Returns an array that contains all elements of the current array except a specific value.
 * @param elem
 * @returns {boolean}
 */
Array.prototype.remove = function(value) {
    return this.filter((e) => e !== value);
};

/**
 * Returns a randomly chosen data sample from array, whereas an element may be chosen multiple times.
 * @param sampleSize (optional) the size of the resulting sample; defaults to the size of the original array.
 * @returns {{indices: Array, values: Array}}
 */
Array.prototype.resample = function(sampleSize) {
    if (typeof sampleSize === 'undefined') {
        sampleSize = this.length;
    }
    let sample = [];
    let sampleIndices = [];
    for (let i=0; i<sampleSize; i++) {
        let ri = Math.randomInt(0, this.length-1);
        sample.push(this[ri]);
        sampleIndices.push(ri);
    }
    return {
        indices: sampleIndices,
        values: sample
    };
};

/**
 * Returns a random subset, where elements are in arbitrary order.
 * Use `seedrandom(seed, { global: true })` for predictable results.
 *
 * @returns {Array}
 */
Array.prototype.randomSubset = function(n) {
    if (n > this.length) {
        throw new Error("`n` must be less or equal to array's length");
    } else if (n == this.length) {
        return this.clone();
    } else {
        return _.sample(this, n);
    }
};

/**
 * Returns a random integer in a given range.
 * Use `seedrandom(seed, { global: true })` for predictable results.
 *
 * @param min the lower bound of the range
 * @param max the upper bound of the range
 * @returns {int} a random integer in the given range.
 */
Math.randomInt = function(min, max) {
    return Math.round(Math.random() * (max - min) + min);
};

/**
 * Calculates the logarithm dualis (base 2).
 * @param val the value
 */
Math.ld = function(val) {
    return Math.log(val) / Math.log(2);
};

/**
 * Fips a coin and returns true for head, false for tail.
 * @param p the probability for heads (defaults to 0.5)
 * @returns {boolean}
 */
function flipCoin(p) {
    if (typeof p === 'undefined') {
        p = 0.5;
    }
    return Math.random() <= p;
}

/**
 * Rounds a number.
 * @param [places=0] the number of digits behind the comma.
 * @returns {number} the rounded number
 */
Number.prototype.round = function(decimals) {
    if (typeof decimals === "undefined") {
        return Math.round(this);
    } else {
        return +(Math.round(this + "e+" + decimals) + "e-" + decimals);
    }
};
