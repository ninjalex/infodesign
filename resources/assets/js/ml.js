"use strict";

/**
 * Trains a single tree for a random forest ML classifier.
 * 
 * @param {Object} data the training data
 * @param {*} [seed] the seed value
 * @returns {Object} the trained tree
 */
function trainRfTree(data, seed) {
    if (typeof seed !== 'undefined') {
        // set seed for pedictable results
        seedrandom(seed, { global: true });
    }

    // perform resampling of input data
    var resampledData = $.extend({}, data, {
        data: data.data.resample().values
    });

    return _trainRfTreeRecursive(resampledData);
}

/**
 * Performs the recursive training of a single tree for a random forest ML classifier.
 *
 * @param {Object} data the training data
 * @param {Object} root the (current) root of the trained tree
 * @returns {Object} the trained tree
 */
function _trainRfTreeRecursive(data, root) {
    root = root || {};

    // consider only a random subsample of available attributes
    let numAttrs = Math.ceil(Math.sqrt(data.attributes.length));
    var attrs = data.attributes.randomSubset(numAttrs);

    // determine attribute for best split
    let maxInfoGain = -1;
    let maxInfoGainAttr = null;
    for (let attr of attrs) {
        let infoGain = calculateInformationGain(data.data, attr, data.goal);
        if (maxInfoGain < infoGain) {
            maxInfoGain = infoGain;
            maxInfoGainAttr = attr;
        }
    }

    // add node to tree
    root.id = _.uniqueId();
    root.label = maxInfoGainAttr;
    root.type = Symbol.for("Node");
    root.edges = {};

    // perform split
    let splits = getSplitsForAttribute(data.data, maxInfoGainAttr, data.goal);
    for (let split of splits) {
        let filteredData = $.extend({}, data, {
            'data' : filterDataSetByAttribute(data.data, maxInfoGainAttr, split.check),
            'attributes' : data.attributes.remove(maxInfoGainAttr)
        });

        if (isPure(filteredData.data)) {
            root.edges[split.label] = {
                id: _.uniqueId(),
                label: split.label,
                rule: split.check,
                node: {
                    id: _.uniqueId(),
                    label: filteredData.data[0][data.goal],
                    type: Symbol.for("Leaf"),
                    parent: root
                }
            };
        } else {
            let child = {
                parent: root
            };
            root.edges[split.label] = {
                id: _.uniqueId(),
                label: split.label,
                rule: split.check,
                node: child
            };
            _trainRfTreeRecursive(filteredData, child);
        }
    }
    return root;
}

/**
 * Calculates the information gain when splitting a data set on a given attribute.
 *
 * @param dataSet an array of data samples whose elements are indexed by the attribute name.
 * @param attribute the attribute key of the attribute to split on
 * @param goalAttribute the goal attribute
 * @returns {Number}
 */
function calculateInformationGain(dataSet, attribute, goalAttribute) {
    // calculate entropy before split
    let goalCounts = getCountsForCategoricalAttribute(dataSet, goalAttribute);
    let infoGain = calculateEntropy.apply(this, Object.values(goalCounts));

    // calculate entropy for every split
    let splits = getSplitsForAttribute(dataSet, attribute, goalAttribute);
    for (let split of splits) {
        let filteredDataSet = filterDataSetByAttribute(dataSet, attribute, split.check);
        let filteredGoalCounts = getCountsForCategoricalAttribute(filteredDataSet, goalAttribute);
        let p = (filteredDataSet.length / dataSet.length);
        infoGain -= p * calculateEntropy.apply(this, Object.values(filteredGoalCounts));
    }

    return infoGain;
}

/**
 * Returns split information for a given attribute.
 *
 * @param dataSet an array of data samples whose elements are indexed by the attribute name.
 * @param attribute the attribute key
 * @param goalAttribute (optional) the goal attribute key; defaults to last attribute of first sample
 * @returns {[{ label: String, check: Function }]}
 */
function getSplitsForAttribute(dataSet, attribute, goalAttribute) {
    goalAttribute = goalAttribute || Object.keys(dataSet[0]).last();

    var splits;
    if (getAttributeType(dataSet, attribute) === Symbol.for("numerical")) {
        var bins = getBinsForNumericalAttribute(dataSet, attribute, goalAttribute);
        splits = bins.map(function(elem) {
            return {
                'attr':  attribute,
                'label': elem['label'],
                'check': elem['contains']
            }
        });
    } else {
        splits = [];
        var counts = getCountsForCategoricalAttribute(dataSet, attribute);
        var distinctValues = Object.keys(counts);
        for (let distinctValue of distinctValues) {
            splits.push({
                'attr':  attribute,
                'label': `== ${distinctValue}`,
                'check': (function(dv) {
                    return function(val) {
                        return val.toString() === dv.toString();
                    }
                })(distinctValue)
            });
        }
    }
    return splits;
}

/**
 * Determines if a data set is pure, i.e. the goal class has only one value.
 *
 * @param dataSet an array of data samples whose elements are indexed by the attribute name.
 * @param goalAttribute (optional) the goal attribute key; defaults to last attribute of first sample
 * @returns {Boolean} true if the data set is pure already, false otherwise.
 */
function isPure(dataSet, goalAttribute) {
    goalAttribute = goalAttribute || Object.keys(dataSet[0]).last();

    let goalCounts = getCountsForCategoricalAttribute(dataSet, goalAttribute);
    let total = Object.values(goalCounts).sum();
    for (let i in goalCounts) {
        if (!goalCounts.hasOwnProperty(i)) {
            continue;
        }
        if (goalCounts[i] === total) {
            return true;
        }
    }
    return false;
}

/**
 * Determines the type of an attribute.
 *
 * @param dataSet an array of data samples whose elements are indexed by the attribute name.
 * @param attribute the attribute key
 * @returns {Symbol} a symbol determining the attribute type:
 *     - Symbol.for("numerical")
 *     - Symbol.for("categorical")
 */
function getAttributeType(dataSet, attribute) {
    for (let sample of dataSet) {
        if (!_.isNumber(sample[attribute])) {
            return Symbol.for("categorical");
        }
    }
    return Symbol.for("numerical");
}

/**
 * Returns the values of a data set for a given attribute.
 *
 * @param dataSet an array of data samples whose elements are indexed by the attribute name.
 * @param attribute the attribute key
 * @returns {Array} the attribute's values.
 */
function getAttributeValues(dataSet, attribute) {
    let values = [];
    for (let sample of dataSet) {
        values.push(sample[attribute]);
    }
    return values;
}

/**
 * Filters the data set by values for a given attribute.
 *
 * @param dataSet an array of data samples whose elements are indexed by the attribute name.
 * @param attribute the attribute key
 * @param callback the filter callback; if it returns true for a value, the sample is collected.
 */
function filterDataSetByAttribute(dataSet, attribute, callback) {
    let filteredSamples = [];
    for (let sample of dataSet) {
        if (callback(sample[attribute])) {
            filteredSamples.push(sample);
        }
    }
    return filteredSamples;
}

/**
 * Returns value counts for a categorical attribute.
 *
 * @param dataSet an array of data samples whose elements are indexed by the attribute name.
 * @param attribute the attribute key
 * @returns {Object}
 */
function getCountsForCategoricalAttribute(dataSet, attribute) {

    var counts = {};
    for (let sample of dataSet) {
        let value = sample[attribute].toString();
        let distinctValues = [];
        if (!(value in counts)) {
            counts[value] = 1;
        } else {
            distinctValues.push(value);
            counts[value]++;
        }
    }
    return counts;
}

/**
 * Returns bins for a numeric attribute.
 *
 * @param dataSet an array of data samples whose elements are indexed by the attribute name.
 * @param attribute the attribute key
 * @param goalAttribute (optional) the goal attribute key; defaults to last attribute of first sample
 * @returns {[{ label: String, contains: Function}]}
 */
function getBinsForNumericalAttribute(dataSet, attribute, goalAttribute) {
    if (getAttributeType(dataSet, attribute) !== Symbol.for("numerical")) {
        throw new Error("can bin numerical attributes only!");
    }
    goalAttribute = goalAttribute || Object.keys(dataSet[0]).last();

    // TODO: implement more sophisticated binning (future work ;-))

    var values = getAttributeValues(dataSet, attribute);
    var mean = values.sum() / values.length;
    mean = mean.round(2);
    return [
        {
            'label': `<= ${mean}`,
            'contains': (function(m) {
                return function(val) {
                    return val <= m;
                }
            })(mean)
        },
        {
            'label': `> ${mean}`,
            'contains': (function(m) {
                return function(val) {
                    return val > m;
                }
            })(mean)
        }
    ];
}

/**
 * Calculates the entropy of a given set of counts.
 */
function calculateEntropy() {
    let counts = Array.prototype.slice.call(arguments);
    let h = 0;
    let total = counts.sum();
    for (let count of counts) {
        h += -((count / total) * Math.ld(count / total));
    }
    return h;
}

module.exports = {
    "calculateInformationGain": calculateInformationGain,
    "getSplitsForAttribute":    getSplitsForAttribute,
    "filterDataSetByAttribute": filterDataSetByAttribute,
    "isPure":                   isPure,
    "trainRfTree":              trainRfTree
};
