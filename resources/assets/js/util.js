"use strict";

/**
 * Reads a data set in JSON format and calls
 * the callback function once a response is
 * available.
 *
 * @param file path to the data set in json format
 * @param callback function to call once the request has been processed
 */
function readDataFromFile(file, callback) {
    let rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType('application/json');
    rawFile.open('GET', file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == '200') {
            callback(rawFile.responseText);
        }
    };
    rawFile.send(null);
}

module.exports = {
    'readDataFromFile': readDataFromFile
};
